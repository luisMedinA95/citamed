import { Component } from '@angular/core';
import { Platform, MenuController, NavController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Inicio',
      url: '/tab1',   //home
      icon: 'home'
    }
    /*,
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }*/
  ];
  aux_val;
  constructor(private platform: Platform, private splashScreen: SplashScreen, private navCtrl: NavController,
    private statusBar: StatusBar, private menu: MenuController, private screenOrientation: ScreenOrientation,
    private toastCtrl: ToastController) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.splashScreen.hide();
      this.statusBar.show();
    });
    
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.screenOrientation.onChange().subscribe(
      () => { /**/});

      let activo_reg = localStorage.getItem('activo_registrar');
      if(activo_reg === '1'){
        this.navCtrl.navigateForward('/tab1');
      }else{
        let activo_ent = localStorage.getItem('activo_entrar');
        if(activo_ent === '2'){
          this.navCtrl.navigateForward('/tab1');
        }else{
          let activo_face = localStorage.getItem('activo_facebook');
          if(activo_face === '3'){
            this.navCtrl.navigateForward('/tab1');
          }else{
            let activo_goog = localStorage.getItem('activo_google');
            if(activo_goog === '4'){
              this.navCtrl.navigateForward('/tab1');
            }else{
              this.navCtrl.navigateForward('/register');
            }
          }
        }
      }
  }

  exit(){
    this.menu.close();
  }
  //chrome://inspect/#devices
  
  close_application(){
    this.aux_val = localStorage.getItem('obj_usr');
    localStorage.removeItem('obj_usr');
    localStorage.removeItem('activo_entrar');
    localStorage.removeItem('activo_registrar');
    localStorage.removeItem('activo_facebook');
    localStorage.removeItem('activo_google');

    
    this.menu.close();
    this.menu.enable(false);
    this.navCtrl.navigateForward('/register');
    localStorage.setItem('aux',this.aux_val);
  }
}
