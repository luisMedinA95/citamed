import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormasPagoPage } from './formas-pago.page';

const routes: Routes = [
  {
    path: '',
    component: FormasPagoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormasPagoPageRoutingModule {}
