import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Platform, NavController, MenuController, ToastController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  @Input() texto='Cargando...';
  @Input() ocultar;
  ocul= false;
  loader;
  mensaje = 'Para avanzar es importante actualizar tu perfil';
  home = true;
  update_perfil = false;
  edt_p = "<";
  desv_cuentas = false;
  vinc_cuentas = false;
  default_imagen = true;
  profile_text = true;
  profile_update = false;
  constructor(private http: HttpClient, private toastCtrl: ToastController, private menu: MenuController, private navCtrl: NavController,
    private socialSharing: SocialSharing, private camera: Camera, private dom: DomSanitizer, private screenOrientation: ScreenOrientation) {}
  ngOnInit(){
    console.log(this.screenOrientation.type);
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.screenOrientation.onChange().subscribe(
      () => {
          console.log("Cambio");
      });

    this.vinc_cuentas = true;
    this.desv_cuentas = false;
    this.menu.enable(false);
    //this.actualizar_perfil();
    this.ocul = true;
  }
  opc_perfil(){
    console.log("Opc");
  }
  
  acept(){
    this.update_perfile();
  }
  cancel(){
    const val = '1';
    localStorage.setItem('cancelo',val);
    console.log("L",localStorage.getItem('cancelo'));
    this.navCtrl.navigateForward('/register');
  }

  async actualizar_perfil() {
    const toast = await this.toastCtrl.create({
      message: this.mensaje,
      //duration: 30000,
      position: "top",   //middle
      cssClass: 'toas_notif',
      buttons: [
        {
          side: 'start',
          text: 'Aceptar',
          handler: () => {
            //redirigir
            this.update_perfile();
          }
        }, {
          //text: 'Cancelar',
          //role: 'cancel',
          handler: () => {
            console.log('Cancelar');
          }
        }
      ]
    });
    toast.present();
  }
  update = {
    nombre: '',
    correo: '',
    password: ''
  }
  obj;
  objeto;
  update_perfile(){
    this.ocul = false;
    this.loader = true;
   setTimeout(() => {
     this.home = false;
     this.update_perfil = true;
     this.loader = false;
     this.texto = 'Loading...';
    }, 3000);
    this.obj = localStorage.getItem('obj_usr');
    this.objeto = JSON.parse(this.obj);
    console.log("Que traes: ",this.objeto);
    this.update.nombre = this.objeto.nombre;
    this.update.correo = this.objeto.mail;
    this.update.password = this.objeto.password;
  }
  
  imagen_base: string = null;
  getPicture(){
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture( options )
    .then(imageData => {
      this.default_imagen = false;
      this.profile_text = false;
      this.profile_update = true;
      this.imagen_base = `data:image/jpeg;base64,${imageData}`;
      //console.log("Img base: ",this.image_base);
    })
    .catch(error =>{
      console.error( error );
    });
  }
  perfiles(){
    this.navCtrl.navigateForward('/tab1');
  }

  Shr(){
    // Check if sharing via email is supported
    /*this.socialSharing.canShareViaEmail().then(() => {
    }).catch(() => {
    });

    this.socialSharing.shareViaEmail('Body', 'Subject', ['luism3039@gmail.com']).then(() => {   //recipient@example.org
    }).catch(() => {
    });*/


    /*this.socialSharing.shareViaFacebook('Facebook').then(() =>{
    }).catch(() => {
    });*/

    /*this.socialSharing.shareViaInstagram('Instagram','url_Instagram').then(()=>{
    }).catch(() => {
    });*/

    /*this.socialSharing.shareViaWhatsApp('Prueba de estado enviado desde App').then(()=>{
    }).catch(() => {
    });*/

    this.socialSharing.share('Compartir', 'App Citas', null, this.share_inpt).then(()=>{
    }).catch(() => {
    });

    /*this.socialSharing.shareViaSMS('SMS', '5615548358').then(()=>{
    }).catch(() => {
    });*/
  }
  share_inpt;
  des_cuenta(){
    this.desv_cuentas = true;
    this.vinc_cuentas = false;
  }
  vinc_cuenta(){
    this.desv_cuentas = false;
    this.vinc_cuentas = true;
  }
}




//https://fotos.subefotos.com/d83e3a8b45dfb0e113dae60d6b98bbb2o.png
  //https://fotos.subefotos.com/ac43b052ad34f170738b80fa2c65261ao.png
/*{
  "wasTapped": 'false', "title": 'Citamed', "body": 'Nuva notificacion'
}*/
