import { Component, OnInit, Input } from '@angular/core';
import { Platform, NavController, MenuController, ToastController } from '@ionic/angular';
import { FCM, NotificationData } from '@ionic-native/fcm/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  @Input() texto = 'Cargando...';
  @Input() sms = '¡Campos Incompletos!';
  @Input() sms_pass = '¡Contraseñas Diferentes!';
  @Input() sms_user_inex = '¡El usuario no existe!';
  @Input() sms_user_exist = '¡El usuario ya existe!';
  @Input() sms_mail_goog_inexist = '¡No existe registro con ese correo!';
  loader;
  band_camp_incomp = false;
  band_pass_incom = false;
  band_user_inexist = false;
  band_regis_goo_inexis = false;
  windhome = false;
  home = false;
  register = false;
  welcome = false;
  token;
  titulo;
  confimar_pass;
  datos = {
    nombre: '',
    apellido: '',
    mail: '',
    password: ''
  }
  login = {
    mail: '',
    password: ''
  }
  userdata;
  coord_lat;
  coord_lng;
  imagen_bd;
  constructor(private menu: MenuController, private geo: Geolocation, private fcm: FCM, private http: HttpClient,
    private toastCtrl: ToastController, private navCtrl: NavController, private fb: Facebook, private camera: Camera,
    private screenOrientation: ScreenOrientation, private googlePlus: GooglePlus) { }
  //url foto
  //https://fotos.subefotos.com/9ac160fffa07cd6b06cb1361011c63f1o.png
  
  ngOnInit() {
    console.log(this.screenOrientation.type);
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.screenOrientation.onChange().subscribe(
      () => {
        console.log("Cambio");
      });

    if (localStorage.getItem('cancelo') === '1') {
      this.login.password === null;
      this.login.mail === null;
    }

    this.token_notificacion();
    this.windhome = true;
    this.menu.enable(false);
  }

  foto_bd() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      this.imagen_bd = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      //Error
    });
  }
  
  user: any = {};
  register_google(){
    this.googlePlus.login({})
    .then(res => {
      localStorage.setItem('logeo_con_google',JSON.stringify(res));
      //this.user = res;
      console.log("Datos: "+"Nombre: "+res.displayName+"Correo: "+res.email);
      let dato = '4';
      localStorage.setItem('activo_google',dato);
      this.navCtrl.navigateForward('/tab1');

      const datos_goog = localStorage.getItem('logeo_con_google');
      const datos_parce_goog = JSON.parse(datos_goog);
      console.log("Google-->: ",datos_goog);
      console.log("Goog parce-->: ",datos_parce_goog);
      /*{
        "accessToken":"ya29.a0Ae4lvC3ymxRO2YWm8nrg-fQrwZbHTeMuLRCDeitJydDkW8DI_3a5nJs3kv43rhkjHl_AKLMBE1phmx-cIlHcvl8TPETJXEaOmi_48uYTYKOurpCacLBsPbWwG4lVoRhrHAlDnE8RUkDyT2YNG3cbKzcnrcrIGh_I0ZVf",
        "expires":1587244023,
        "expires_in":1575,
        "email":"luism3039@gmail.com",
        "userId":"103680877321956234651",
        "displayName":"Luis Medina",
        "familyName":"Medina",
        "givenName":"Luis"
      }*/
    })
    .catch(err => console.error(err));
  }
  entrar_google(){
    this.googlePlus.login({})
    .then(res => {
      const datos_goog = localStorage.getItem('logeo_con_google');
      const datos_parce_goog = JSON.parse(datos_goog);

      if(datos_parce_goog.email === res.email){
        let dato = '5';
        localStorage.setItem('activo_entrar_google',dato);
        this.navCtrl.navigateForward('/tab1');
      }else{
        this.band_regis_goo_inexis = true;
        setTimeout(() => {
          this.band_regis_goo_inexis = false;
          this.sms_mail_goog_inexist = '¡No existe registro con ese correo!';
        }, 1000);
      }
    })
    .catch(err => console.error(err));
  }

  register_facebook() {
    this.fb.login(['public_profile','email'])    //'email', 'user_friends',
      .then((res: FacebookLoginResponse) => {
        console.log("Datos: ", this.fb);
        if (res.status === 'connected') {
          this.user.img = 'https://graph.facebook.com/' + res.authResponse.userID + '/picture?type=square';
          console.log("Token de Facebook: ", res.authResponse.accessToken);
          this.getData(res.authResponse.accessToken);
        } else {
          console.log("Error de Logeo");
        }
        console.log('Login Facebook', res);
        let dato = '3'
        localStorage.setItem('activo_facebook',dato);
      })
      .catch(e => console.log('Catch Error', e));
      //no hay internet
  }
  getData(access_token: string) {
    let url = 'https://graph.facebook.com/me?fields=id,name,first_name,last_name,email&access_token=' + access_token;   //last_name,email
    this.http.get(url).subscribe(data => {
      this.userdata = data;
      localStorage.setItem('logeo_con_facebook',JSON.stringify(data));
      this.navCtrl.navigateForward('/tab1');


      const datos_face = localStorage.getItem('logeo_con_facebook');
      const datos_parceados_face = JSON.parse(datos_face);
      console.log("Facebook-->: ",datos_face);
      console.log("Face parce-->: ",datos_parceados_face);
      /*
      {
        "id":"180678440044125",
        "name":"Luis Medina",
        "first_name":"Luis",
        "last_name":"Medina"
      }
      */
    })
  }
  logout_facebook(){
    this.fb.logout();
  }

  band_camera = false;
  band_camera_imag = true;
  auxiliar;
  valores_aux;
  mail_aux;
  band_usuario_exist = false;
  registrar() {
    if (this.datos.nombre === null || this.datos.nombre === undefined || this.datos.nombre === '' || this.datos.apellido === null || this.datos.apellido === undefined || this.datos.apellido === ''
      || this.datos.mail === null || this.datos.mail === undefined || this.datos.mail === '' || this.datos.password === null || this.datos.password === undefined || this.datos.password === ''
      || this.confimar_pass === null || this.datos.password === undefined || this.datos.password === '') {
      this.band_camp_incomp = true;
      setTimeout(() => {
        this.band_camp_incomp = false;
        this.sms = '¡Campos Incompletos!';
      }, 1000);
    } else {
      this.auxiliar = localStorage.getItem('aux');
      if (this.auxiliar === null) {
        if (this.datos.password === this.confimar_pass) {
          if (this.imagen_bd === null || this.imagen_bd === undefined || this.imagen_bd === '') {
            this.band_camp_incomp = true;
            this.band_camera_imag = false;
            this.band_camera = true;
            setTimeout(() => {
              this.band_camp_incomp = false;
              this.band_camera = false;
              this.band_camera_imag = true;
              this.sms = '¡Campos Incompletos!';
            }, 1000);
          } else {
            let dato = '1';
            localStorage.setItem('activo_registrar', dato);
            const dat = JSON.stringify(this.datos);
            localStorage.setItem('obj_usr', dat);
            console.log("Quien se registro: ", JSON.parse(dat));
            this.loader = true;
            setTimeout(() => {
              this.loader = false;
              this.texto = 'Cargando...';
              this.navCtrl.navigateForward('/home');
              this.datos.nombre = null;
              this.datos.mail = null;
              this.datos.password = null;
              this.datos.apellido = null;
              this.confimar_pass = null;
              this.imagen_bd = null;
            }, 2500);
          }
        } else {
          this.band_pass_incom = true;
          setTimeout(() => {
            this.band_pass_incom = false;
            this.sms_pass = '¡Contraseñas Diferentes!';
          }, 1000);
        }
      } else {
        this.valores_aux = JSON.parse(this.auxiliar);
        console.log("AUX Ya registrado: ",this.valores_aux);
        this.mail_aux = this.valores_aux.mail;
        console.log("Mail que se va repetir: ",this.mail_aux);
        if(this.mail_aux === this.datos.mail){
          this.band_usuario_exist = true;
          setTimeout(() => {
            this.band_usuario_exist = false;
            this.sms_user_exist = '¡El usuario ya existe!';
          }, 1000);
        }else{
          this.band_user_inexist = true;
          setTimeout(() => {
            this.band_user_inexist = false;
            this.sms_user_inex = '¡El usuario no existe!';
          }, 1000);
        }
      }
    }
  }

  valores;
  mail;
  password;
  registro_previo;
  entre() {
    if (this.login.mail === null || this.login.mail === '' || this.login.mail === undefined || this.login.password === null ||
      this.login.password === '' || this.login.password === undefined) {
      this.band_camp_incomp = true;
      setTimeout(() => {
        this.band_camp_incomp = false;
        this.sms = '¡Campos Incompletos!';
      }, 1000);
    } else {
      this.registro_previo = localStorage.getItem('aux');
      if (this.registro_previo === null) {
        this.band_user_inexist = true;
        setTimeout(() => {
          this.band_user_inexist = false;
          this.sms_user_inex = '¡El usuario no existe!';
        }, 1000);
      } else {
        this.valores = JSON.parse(this.registro_previo);
        this.mail = this.valores.mail;
        this.password = this.valores.password;
        if (this.login.mail === this.mail && this.login.password === this.password) {
          this.navCtrl.navigateForward('/tab1');
          let dato = '2';
          localStorage.setItem('activo_entrar', dato);
        } else {
          this.band_user_inexist = true;
          setTimeout(() => {
            this.band_user_inexist = false;
            this.sms_user_inex = '¡El usuario no existe!';
          }, 1000);
        }
      }
    }
  }

  inicio() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.windhome = false;
      this.home = true;
    }, 1800);
  }
  entrar() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.home = false;
      this.welcome = true;
    }, 1800);
  }
  register_welcome() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.register = false;
      this.welcome = true;
      this.datos.nombre = null;
      this.datos.apellido = null;
      this.datos.mail = null;
      this.datos.password = null;
      this.confimar_pass = null;
      this.imagen_bd = null;
    }, 1800);
  }
  welcome_register() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.welcome = false;
      this.register = true;
      this.login.mail = null;
      this.login.password = null;
    }, 1800);
  }
  registro() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.register = true;
      this.home = false;
    }, 1800);
  }

  token_notificacion() {
    this.fcm.getToken().then(token => {
      this.token = token;
      console.log("Token: ", this.token);
    });
    this.fcm.onNotification().subscribe((data: NotificationData) => {
      localStorage.setItem('traer', JSON.stringify(data));
      console.log("Obtengo Data:", data);
      if (data.wasTapped) {
        this.titulo = "if";
        this.alerta_notificacion(this.titulo);
        alert("If: " + JSON.stringify(data));
      } else {
        this.titulo = "else";
        this.alerta_notificacion(this.titulo);
        alert("Else: " + JSON.stringify(data));
      }
    }, error => {
      console.error("Error en notificacion", error)
    });
  }

  async alerta_notificacion(title) {
    const toast = await this.toastCtrl.create({
      message: 'Notificacion: ' + title,
      duration: 30000,
      position: "bottom",
      buttons: [
        {
          side: 'start',
          text: 'Aceptar',
          handler: () => {
            //redirigir
          }
        }, {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelar');
          }
        }
      ]
    });
    toast.present();
  }

  typepass = 'password';
  eye_closed = true;
  eye_open = false;
  showPass() {
    if (this.typepass == 'password') {
      this.typepass = 'text';
      this.eye_open = true;
      this.eye_closed = false;
    } else {
      this.typepass = 'password';
      this.eye_closed = true;
      this.eye_open = false;
    }
  }
  showPass2() {
    if (this.typepass == 'password') {
      this.typepass = 'text';
      this.eye_open = true;
      this.eye_closed = false;
    } else {
      this.typepass = 'password';
      this.eye_closed = true;
      this.eye_open = false;
    }
  }
  typepas = 'password';
  eye_clos = true;
  eye_op = false;
  hide_Pass() {
    if (this.typepas == 'password') {
      this.typepas = 'text';
      this.eye_op = true;
      this.eye_clos = false;
    } else {
      this.typepas = 'password';
      this.eye_clos = true;
      this.eye_op = false;
    }
  }
  mostrar_Pass() {
    if (this.typepas == 'password') {
      this.typepas = 'text';
      this.eye_op = true;
      this.eye_clos = false;
    } else {
      this.typepas = 'password';
      this.eye_clos = true;
      this.eye_op = false;
    }
  }

  coordenadas() {
    this.geo.getCurrentPosition().then((geoposition: Geoposition) => {
      console.log("Cord", geoposition);
      this.coord_lat = geoposition.coords.latitude;
      this.coord_lng = geoposition.coords.longitude;
    }).catch((error) => {
      console.log("Error", error);
    })
  }
}
