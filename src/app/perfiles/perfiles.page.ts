import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
@Component({
  selector: 'app-perfiles',
  templateUrl: './perfiles.page.html',
  styleUrls: ['./perfiles.page.scss'],
})
export class PerfilesPage implements OnInit {
  usuarios = [
    {
      stateId: '1',
      name_State: 'Aguascalientes',
      data: [
        {
          usuarioId: '1',
          name: 'Lizbeth',
          last_name: 'Felipe,',
          years: '26 años,',
          status: 'Soltera',
        },
        {
          usuarioId: '2',
          name: 'Viridiana',
          last_name: 'Garcia,',
          years: '22 años,',
          status: 'Viuda',
        },
        {
          usuarioId: '3',
          name: 'Edith',
          last_name: 'Moreno,',
          years: '25 años,',
          status: 'Soltera',
        }
      ]
    },
    {
      stateId: '2',
      name_State: 'Baja California Sur',
      data: [
        {
          usuarioId: '1',
          name: 'Alondra',
          last_name: 'Lopez,',
          years: '23 años,',
          status: 'Casada',
        },
        {
          usuarioId: '2',
          name: 'Ines',
          last_name: 'Martinez,',
          years: '26 años,',
          status: 'Soltera',
        },
        {
          usuarioId: '3',
          name: 'Elizabeth',
          last_name: 'De Jesus,',
          years: '24 años,',
          status: 'Soltera',
        },
        {
          usuarioId: '4',
          name: 'Diana',
          last_name: 'Garcia,',
          years: '22 años,',
          status: 'Soltera',
        }
      ]
    },
    {
      stateId: '3',
      name_State: 'Campeche',
      data: [
        {
          usuarioId: '1',
          name: 'Karina',
          last_name: 'Valdez,',
          years: '24 años,',
          status: 'Soltera',
        }
      ]
    },
    {
      stateId: '4',
      name_State: 'Coahuila',
      data: [
        {
          usuarioId: '1',
          name: 'Guadalupe',
          last_name: 'Alonso,',
          years: '25 años,',
          status: 'Juntada',
        },
        {
          usuarioId: '2',
          name: 'Yeretzy',
          last_name: 'Garcia,',
          years: '20 años,',
          status: 'Casada',
        }
      ]
    },
    {
      stateId: '5',
      name_State: 'Chiapas',
      data: [
        {
          usuarioId: '1',
          name: 'Guadalupe',
          last_name: 'Cruz,',
          years: '20 años,',
          status: 'Soltera',
        },
        {
          usuarioId: '2',
          name: 'Patricia',
          last_name: 'Gonzalez,',
          years: '26 años,',
          status: 'Soltera',
        },
        {
          usuarioId: '3',
          name: 'Lizeth',
          last_name: 'Pedraza,',
          years: '22 años,',
          status: 'Casada',
        },
        {
          usuarioId: '4',
          name: 'Karen',
          last_name: 'Montiel,',
          years: '21 años,',
          status: 'Soltera',
        }
      ]
    },
    {
      stateId: '6',
      name_State: 'Estado de México',
      data: [
        {
          usuarioId: '1',
          name: 'Luis Enrique',
          last_name: 'Medina,',
          years: '24 años,',
          status: 'Soltero',
        },
        {
          usuarioId: '2',
          name: 'Diana',
          last_name: 'Camilo,',
          years: '20 años,',
          status: 'Soltera',
        }
      ]
    } 
  ]
  usuario;
  data_user;
  imagen_perf;
  constructor(private menu: MenuController, private navCtrl: NavController, private screenOrientation: ScreenOrientation) { }

  ngOnInit() {
    console.log(this.screenOrientation.type);
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.screenOrientation.onChange().subscribe(
      () => {
          console.log("Cambio");
      });

    this.menu.enable(true);
    for(let i=0; i<this.usuarios.length; i++){
      this.usuario = this.usuarios[i];
      console.log("User: ",this.usuario);
      for(let z=0; z<this.usuario.data.length; z++){
        this.data_user = this.usuario.data[z];
        console.log("data: ",this.data_user);
      }
    }
  }

  imagen(){
    this.imagen_perf = '<img src="/assets/icon.png" alt="cargando..." />'
  	document.getElementById('imagencargando').innerHTML = this.imagen_perf;
  }

  Datos(val){
    alert(JSON.stringify(val));
  }

  cambio(){
    console.log("Cambio");
  }

}
